"use client";

import { configureStore } from "@reduxjs/toolkit";
import appSetting from "../redux/slices/app-settings";
import { usersApi, userRolesApi } from "../redux/slices/users";

export const store = configureStore({
  reducer: {
    setting: appSetting,
    [usersApi.reducerPath]: usersApi.reducer,
    [userRolesApi.reducerPath]: userRolesApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(usersApi.middleware, userRolesApi.middleware),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
