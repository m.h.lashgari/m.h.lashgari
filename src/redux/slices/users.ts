"use client";
import { getFromLocalStorage } from "./../../app/utility/getToken";

import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

interface User {
  id: string;
  name: string;
}

interface UsersResponse {
  data: { users: { results: User[]; total: number } };
}

interface UsersOptions {
  // options here
}

interface UserRolesResponse {
  data: { userRoles: { roles: string[] } };
}

interface UserRolesOptions {
  id: string;
}

export const usersApi = createApi({
  reducerPath: "users",
  baseQuery: fetchBaseQuery({ baseUrl: "http://2.56.154.227:7000/graphql" }),
  endpoints: (builder) => ({
    getUsers: builder.query<UsersResponse, UsersOptions>({
      query: (options) => ({
        url: "",
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`, // add token to the header
        },
        body: {
          query: `
          query getUsers{
            users(options:{}){
              total,
              results{
                name,id,email
              }
            }
          }
          `,
          variables: {
            options,
          },
        },
      }),
    }),
  }),
});

export const { useGetUsersQuery } = usersApi;

export const userRolesApi = createApi({
  reducerPath: "userRoles",
  baseQuery: fetchBaseQuery({ baseUrl: "http://2.56.154.227:7000/graphql" }),
  endpoints: (builder) => ({
    getRoles: builder.query<UserRolesResponse, UserRolesOptions>({
      query: (id) => ({
        url: "",
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`, // add token to the header
        },
        body: {
          query: `
          query getUserRoles($id: String!) {
            userRoles(id: $id) {
              roles
            }
          }
          `,
          variables: {
            id,
          },
        },
      }),
    }),
  }),
});

export const { useGetRolesQuery } = userRolesApi;
