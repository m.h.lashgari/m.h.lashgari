"use client";
import { getFromLocalStorage } from "./../../app/utility/getToken";

import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface SettingsState {
  language: "en" | "fa";
  theme: "light" | "dark";
}

const initialState: SettingsState = {
  language: getFromLocalStorage("language")
    ? getFromLocalStorage("language") === "english"
      ? "en"
      : "fa"
    : "en",
  theme: "light",
};

export const settingsSlice = createSlice({
  name: "settings",
  initialState,
  reducers: {
    languageSetter: (state, action: PayloadAction<"any">) => {
      state.language = action.payload;
    },
    setTheme: (state, action: PayloadAction<"light" | "dark">) => {
      state.theme = action.payload;
    },
  },
});

export const { languageSetter, setTheme } = settingsSlice.actions;

export default settingsSlice.reducer;
