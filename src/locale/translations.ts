export const translation = {
  en: {
    login: "login",
    signIn: "sign in",
    users: "users",
    user: "user",
  },
  fa: {
    login: "ورود کاربران",
    signIn: "ورود",
    users: "کاربران",
    user: "کاربر",
  },
};
