"use client";

import { useSelector } from "react-redux";
import { translation } from "../locale/translations";

const useTranslation = () => {
  const language = useSelector((state) => state.setting.language);

  console.log({ language });

  const t = (word: string) => {
    return translation?.[language]?.[word];
  };

  return { t };
};

export default useTranslation;
