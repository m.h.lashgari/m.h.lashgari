"use client";

import { getFromLocalStorage } from "@/app/utility/getToken";
import { useState, useEffect } from "react";

type Theme = "light" | "dark";

export const useTheme = (): [Theme, () => void] => {
  const [theme, setTheme] = useState<Theme>(() => {
    const storedTheme = getFromLocalStorage("theme") as Theme;
    if (storedTheme) {
      return storedTheme;
    }
    return "light";
  });

  const toggleTheme = () => {
    const newTheme = theme === "light" ? "dark" : "light";
    localStorage.setItem("theme", newTheme);
    setTheme(newTheme);
  };

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [theme]);

  return [theme, toggleTheme];
};
