"use client";
import { Router } from "next/router";
import Accordion from "../components/elements/Accordion";
import { useGetUsersQuery } from "@/redux/slices/users";
import { useRouter } from "next/navigation";
import ThemeChanger from "../components/elements/ThemeChanger";
import MultiLanguage from "../components/elements/MultiLanguage";
import { useAppSelector } from "@/hooks/redux-hooks";
import useTranslation from "@/hooks/useTranslation";
import { getFromLocalStorage } from "../utility/getToken";

const Users = () => {
  const router = useRouter();

  const { data, isLoading, isError, error } = useGetUsersQuery({});
  const { t } = useTranslation();
  console.log({ error, data, isError });

  if (isLoading) return <div className="dark:text-gray-400 ">Loading...</div>;
  return (
    <div className="w-[50rem] ">
      <div className="sm:mx-auto sm:w-full sm:max-w-md">
        <h2 className="mt-6 text-center flex tracking-tight justify-between">
          <p className="text-2xl font-bold dark:text-gray-400 ">{t("users")}</p>
          <div className="flex align-middle justify-center">
            <ThemeChanger />
            <MultiLanguage />
            <button
              className="ml-2 text-red-600"
              onClick={() => {
                localStorage.removeItem("token");
                router.push("/login");
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75"
                />
              </svg>
            </button>
          </div>
        </h2>
      </div>

      {data?.data?.users?.results?.map((user: any, index: number) => {
        return (
          <Accordion
            name={user.name}
            id={user.id}
            key={user.id}
            email={user.email}
            index={index + 1}
          />
        );
      })}
      <h3 className="flex justify-center dark:text-gray-400 ">
        Please be aware you can only check or unchecked one of them and select
        save or remove the role cause of backend limitation{" "}
      </h3>
    </div>
  );
};

export default Users;
