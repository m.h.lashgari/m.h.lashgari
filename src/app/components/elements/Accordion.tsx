"use client";

import { useGetRolesQuery } from "@/redux/slices/users";
import { gql, useMutation } from "@apollo/client";
import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/20/solid";
import { useEffect, useState } from "react";
import Spinner from "./Spinner";
import useTranslation from "@/hooks/useTranslation";
import { getFromLocalStorage } from "@/app/utility/getToken";

interface Props {
  name: string;
  id: string;
  email: string;
  index?: number | undefined;
}

const ASSIGN_ROLE_MUTATION = gql`
  mutation AssignRole($role: UserRoles!, $userId: String!) {
    assignRole(assignRoleInput: { role: $role, userId: $userId }) {
      roles
    }
  }
`;
const REMOVE_ROLE_MUTATION = gql`
  mutation removeRole($role: UserRoles!, $userId: String!) {
    removeRole(removeRoleInput: { role: $role, userId: $userId })
  }
`;

const Accordion = ({ name, id, email, index }: Props) => {
  const { t } = useTranslation();

  const {
    data: userData,
    isLoading,
    isError,
    isSuccess,
    error: userError,
  } = useGetRolesQuery(id);

  const [role, setRole] = useState("");
  const [checkedRoles, setCheckedRoles] = useState({});

  useEffect(() => {
    if (isSuccess) {
      console.log({ userData });
      setCheckedRoles({
        ADMIN: userData?.data?.userRoles.roles.includes("ADMIN"),
        COURIER: userData?.data?.userRoles.roles.includes("COURIER"),
        CUSTOMER: userData?.data?.userRoles.roles.includes("CUSTOMER"),
        CONTENT_EXPERT:
          userData?.data?.userRoles.roles.includes("CONTENT_EXPERT"),
        CONTENT_MANAGER:
          userData?.data?.userRoles.roles.includes("CONTENT_MANAGER"),
        MAINTAINER: userData?.data?.userRoles.roles.includes("MAINTAINER"),
        SALES_EXPERT: userData?.data?.userRoles.roles.includes("SALES_EXPERT"),
        SALES_MANAGER:
          userData?.data?.userRoles.roles.includes("SALES_MANAGER"),
      });
    }
  }, [isSuccess, userData]);

  const [assignRole, { loading, error, data }] = useMutation(
    ASSIGN_ROLE_MUTATION,
    {
      context: {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      },
    }
  );
  const [
    removeRole,
    { loading: removeLoading, error: removeError, data: removeData },
  ] = useMutation(REMOVE_ROLE_MUTATION, {
    context: {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    },
  });
  const checkBoxList = [
    {
      name: "ADMIN",
      id: 1,
      as: "admin",
      checked: checkedRoles.ADMIN,
    },
    {
      name: "COURIER",
      id: 4,
      as: "courier",
      checked: checkedRoles.COURIER,
    },
    {
      name: "CUSTOMER",
      id: 5,
      as: "customer",
      checked: checkedRoles.CUSTOMER,
    },
    {
      name: "CONTENT_EXPERT",
      id: 2,
      as: "content expert",
      checked: checkedRoles.CONTENT_EXPERT,
    },
    {
      name: "CONTENT_MANAGER",
      id: 3,
      as: "content manager",
      checked: checkedRoles.CONTENT_MANAGER,
    },
    {
      name: "MAINTAINER",
      id: 6,
      as: "maintainer",
      checked: checkedRoles.MAINTAINER,
    },
    {
      name: "SALES_EXPERT",
      id: 8,
      as: "sales expert",
      checked: checkedRoles.SALES_EXPERT,
    },
    {
      name: "SALES_MANAGER",
      id: 9,
      as: "sales manager",
      checked: checkedRoles.SALES_MANAGER,
    },
  ];

  const handleRoleChange = (event: any) => {
    setCheckedRoles((state) => ({
      ...state,
      [event.target.name]: !state[event.target.name],
    }));
    setRole(event.target.name);
  };

  const handleAssignRole = () => {
    assignRole({ variables: { role, userId: id } });
  };
  const handleRemoveRole = () => {
    removeRole({ variables: { role, userId: id } });
  };

  if (isLoading)
    return (
      <div className="flex justify-center align-middle">
        <Spinner />
      </div>
    );

  return (
    <div className="w-full px-4 ">
      <div className="mx-auto w-full max-w-md rounded-2xl bg-white p-2 my-2">
        <Disclosure>
          {({ open }) => (
            <>
              <Disclosure.Button className="flex w-full justify-between rounded-lg bg-purple-300 dark:bg-gray-800 mx-auto max-w-md p-2 text-left text-sm font-medium text-purple-900 dark:text-white hover:bg-purple-200 dark:hover:bg-gray-700 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                <span>{name ?? `${t("user")} ${index} `}</span>
                <span className="ml-3">{email}</span>
                <ChevronUpIcon
                  className={`${
                    open ? "rotate-180 transform" : ""
                  } h-5 w-5 text-purple-500`}
                />
              </Disclosure.Button>
              <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500 bg-purple-100 dark:bg-gray-900  rounded-lg">
                <div className="flex flex-wrap">
                  {" "}
                  {checkBoxList.map((checkbox) => {
                    return (
                      <div
                        className="flex items-center mx-[0.5rem]"
                        key={checkbox.id}
                      >
                        <input
                          checked={checkedRoles[checkbox.name]}
                          name={checkbox.name}
                          type="checkbox"
                          onChange={(e) => handleRoleChange(e)}
                          className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500 dark:text-white dark:focus:ring-offset-gray-900 dark:focus:ring-offset-2 dark:text-green-500"
                        />
                        <label
                          htmlFor="remember-me"
                          className="ml-1 block text-sm text-gray-900 dark:text-white"
                        >
                          {checkbox.as}
                        </label>
                      </div>
                    );
                  })}
                </div>{" "}
                <div className="my-2">
                  <button
                    className="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                    onClick={handleAssignRole}
                  >
                    {loading ? <Spinner /> : <>save</>}
                  </button>
                  <button
                    className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                    onClick={handleRemoveRole}
                  >
                    {removeLoading ? <Spinner /> : <> Remove Role</>}
                  </button>
                </div>
              </Disclosure.Panel>
            </>
          )}
        </Disclosure>
      </div>
    </div>
  );
};

export default Accordion;
