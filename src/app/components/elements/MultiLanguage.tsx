"use client";

import { getFromLocalStorage } from "@/app/utility/getToken";
import { useAppDispatch } from "@/hooks/redux-hooks";
import { languageSetter } from "@/redux/slices/app-settings";
import { useEffect, useState } from "react";

const MultiLanguage = () => {
  const dispatch = useAppDispatch();
  const [language, setLanguage] = useState<string>();

  useEffect(() => {
    const html = document.documentElement;
    const storedLanguage = getFromLocalStorage("language");

    if (storedLanguage) {
      setLanguage(storedLanguage);
      if (storedLanguage === "فارسی") {
        html.setAttribute("dir", "rtl");
      } else {
        html.removeAttribute("dir");
      }
    } else {
      setLanguage("english");
    }
  }, []);

  const handleClick = () => {
    const html = document.documentElement;
    if (html.getAttribute("dir") === "rtl") {
      html.removeAttribute("dir");
      setLanguage("english");
      localStorage.setItem("language", "english");
      dispatch(languageSetter("en"));
    } else {
      html.setAttribute("dir", "rtl");
      setLanguage("فارسی");
      localStorage.setItem("language", "فارسی");
      dispatch(languageSetter("fa"));
    }
  };

  return (
    <button className="dark:text-gray-400 mx-2" onClick={handleClick}>
      {language}
    </button>
  );
};

export default MultiLanguage;
