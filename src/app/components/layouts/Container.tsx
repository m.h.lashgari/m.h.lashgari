"use client";

import { getFromLocalStorage } from "@/app/utility/getToken";
import { useAppDispatch } from "@/hooks/redux-hooks";
import { useTheme } from "@/hooks/useTheme";
import client from "@/lib/apollo-client";
import { store } from "@/redux/store";
import { ApolloProvider } from "@apollo/client";
import { appWithTranslation } from "next-i18next";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { Provider } from "react-redux";

const Container = ({ children }: { children: React.ReactNode }) => {
  const router = useRouter();
  const [] = useTheme();

  useEffect(() => {
    if (!getFromLocalStorage("token")) {
      router.push("/login");
    } else {
      router.push("/users");
    }
  }, [router]);

  return (
    <div className="flex items-center justify-center h-screen dark:bg-gray-900">
      <ApolloProvider client={client}>
        <Provider store={store}>{children}</Provider>
      </ApolloProvider>
    </div>
  );
};

export default Container;
