"use client";
import { gql, useMutation } from "@apollo/client";
import { useState } from "react";
import { useRouter } from "next/navigation";
import ThemeChanger from "../components/elements/ThemeChanger";
import MultiLanguage from "../components/elements/MultiLanguage";
import Toast from "../components/elements/Toast";
import Spinner from "../components/elements/Spinner";
import { useAppSelector } from "@/hooks/redux-hooks";
import useTranslation from "@/hooks/useTranslation";

const LOGIN_MUTATION = gql`
  mutation Login($email: String!, $password: String!) {
    login(authUserInput: { email: $email, password: $password }) {
      isAdmin
      password
      access_token
      id
    }
  }
`;

const Login = () => {
  const router = useRouter();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [login, { data, loading, error }] = useMutation(LOGIN_MUTATION);
  const [notif, setNotif] = useState();

  const { t } = useTranslation();

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      const response = await login({ variables: { email, password } });
      const token = response.data.login.access_token;
      localStorage.setItem("token", token);
      router.push("/users");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {error ? <Toast /> : null}
      <div className="flex flex-col justify-center py-12 sm:px-6 lg:px-8 min-w-[20rem] sm:min-w-[50rem] dark:bg-gray-900">
        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <h2 className="mt-6 text-center flex tracking-tight justify-between">
            <p className="text-2xl font-bold dark:text-gray-400 ">
              {t("login")}
            </p>
            {/* <Toast /> */}
            <ThemeChanger />
            <MultiLanguage />
          </h2>
        </div>

        <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
          <div className="bg-white dark:bg-gray-800 py-8 px-4 drop-shadow-2xl sm:rounded-lg sm:px-10">
            <form className="space-y-6">
              <div>
                <label
                  htmlFor="email"
                  className="block text-sm font-medium text-gray-700 dark:text-gray-400"
                >
                  Email
                </label>
                <div className="mt-1">
                  <input
                    id="email"
                    name="email"
                    value={email}
                    type="email"
                    autoComplete="email"
                    required
                    className="block w-full appearance-none rounded-md border border-gray-300 dark:border-gray-700 px-3 py-2 placeholder-gray-400 dark:placeholder-gray-500 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
              </div>

              <div>
                <label
                  htmlFor="password"
                  className="block text-sm font-medium text-gray-700 dark:text-gray-400"
                >
                  Password
                </label>
                <div className="mt-1">
                  <input
                    id="password"
                    name="password"
                    type="password"
                    value={password}
                    autoComplete="current-password"
                    required
                    className="block w-full appearance-none rounded-md border border-gray-300 dark:border-gray-700 px-3 py-2 placeholder-gray-400 dark:placeholder-gray-500 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
              </div>

              <div className="flex items-center justify-between">
                <div className="text-sm"></div>
              </div>

              <div>
                <button
                  onClick={handleSubmit}
                  className="flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >
                  {loading ? <Spinner /> : <> {t("signIn")}</>}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
