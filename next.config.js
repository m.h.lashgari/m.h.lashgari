/** @type {import('next').NextConfig} */

const nextConfig = {
  typescript: {
    ignoreBuildErrors: true,
  },
  i18n: {
    defaultLocale: "en",
    locales: ["en", "fa"],
  },
  experimental: {
    appDir: true,
  },
};

module.exports = nextConfig;
